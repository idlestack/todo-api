const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

const {ObjectID} = require('mongodb');

var id = '5974eb7159e1da76076eb1d3';

var userID = '5974ace707d1f284056c437';

if (!ObjectID.isValid(userID)) {
    return console.log('ID Is Not Valid');
}

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// });

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//         return console.log('No Document Found');
//     }
//     console.log('Todo', todo);
// });

User.findById(userID).then((user) => {
    if (!user) {
        return console.log('User Not Found');
    }
    console.log(JSON.stringify(user,undefined,2));
}, (err) => {
    console.log('No User Found');
})