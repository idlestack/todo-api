//const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

var ObjID = new ObjectID();
console.log(ObjID);


MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
    if (error) {
        console.log('Unable To Connect To Database')
    }
    else {
        console.log('Connected To TodoApp on Port 27017');
    }

    //find one and update
    db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID('5973e1b5cb72830330776d16')
    }, {
            $set: {
                completed: true
            }
            ,
            $inc: {
                age: 10
            }
        }, {
            returnOriginal: false

        }).then((result) =>  {
            console.log(result);
        });

    //db.close();
});