//const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

var ObjID = new ObjectID();
console.log(ObjID);


MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
    if (error) {
        console.log('Unable To Connect To Database')
    }
    else {
        console.log('Connected To TodoApp on Port 27017');
    }

    // //delete many
    // db.collection('Todos').deleteMany({text: 'Eat Lunch'}).then((result) => {
    //     console.log(result);
    // });

    //delete one
    // db.collection('Todos').deleteOne({}).then((result) => {
    //     console.log('Deleted One Document');
    // });

    //findoneanddelete
    db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
        console.log(result);
    });

    //db.close();
});