//const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

var ObjID = new ObjectID();
console.log(ObjID);


MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, db) => {
    if (error) {
        console.log('Unable To Connect To Database')
    }
    else {
        console.log('Connected To TodoApp on Port 27017');
    }


    db.collection('Todos').find({
        _id: new ObjectID('5973e7522d9015028b35cb76')
        }).count().then((count) => {
            console.log('Todos');
            console.log(`Todos Count: ${count}`);
        }, (err) => {
            console.log('Unable To Get Documents');
        });


    //db.close();
});