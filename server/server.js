console.log('Starting Server.Js');

//const _ = require('lodash');

const express = require('express');
const bodyparser = require('body-parser');

const { mongoose } = require('./db/mongoose');
const { Todo } = require('./models/todo');
const { User } = require('./models/todo');

const { ObjectID } = require('mongodb');

var app = express();
app.use(bodyparser.json())

app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });

    todo.save().then((doc) => {
        res.send(doc);
    }, (error) => {
        res.status(400).send(error);
    });
});

app.get('/todos', (req, res) => {
    Todo.find().then((todos) => {
        res.send({ todos });
    }, (error) => {
        res.status(400).send(error);
    })
});

app.get('/todos/:id', (req, res) => {
    var id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send({});
    } else {
        Todo.findById(id).then((todo) => {
            if (!todo) {
                res.status(404).send({});
            }
            res.send(todo);
        }, (err) => {
            res.status(400).send();
        });
    }
});

app.delete('/todos/:id', (req, res) => {
    var id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findByIdAndRemove(id).Then((todo) => {
        if (!todo) {
            return res.status(404).send();
        }

        res.send(todo);
    }).catch((e) => {
        res.status(400).send();
    })
});

app.patch('/todos/:id', (req, res) => {
    var id = req.params.id;
    console.log('patch');
    //var body = _.pick(req.body, ['text', 'completed']);
    var body = req.body;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    if (body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findByIdAndUpdate(id, { $set: body }, { new: true }).then((todo) => {
        if (!todo) {
            return res.status(404).send();
        }

        res.send({ todo });
    }).catch((e) => {
        res.status(400).send();
    })

})

const PORT = process.env.PORT || 3000;
console.log(PORT);

app.listen(PORT, () => {
    console.log(`started server on Port ${PORT}`);
});


module.exports = { app };
