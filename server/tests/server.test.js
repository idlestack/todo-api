const expect = require('expect');
const request = require('supertest');

const { app } = require('./../server');
const { Todo } = require('./../models/todo');
const {ObjectID} = require('mongodb');

const todos = [
    {text: 'Test Generated Text', _Id: new ObjectID()}, 
    { text: 'Test Generated Text', _Id: new ObjectID()},
    { text: 'Test Generated Text', _Id: new ObjectID()}
];

beforeEach((done) => {
    //Remove All Todo Records
    Todo.remove({}).then(() => {
        return Todo.insertMany(todos);
    }).then(() => done());
});


describe('POST /todos', () => {
    it('should create a new todo', (done) => {
        var text = 'Test Generated Text';
        request(app)
            .post('/todos')
            .send({ text })
            .expect(200)
            .expect((res) => {
                expect(res.body.text).toBe(text);
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                Todo.find().then((todos) => {
                    expect(todos.length).toBe(4);
                    expect(todos[0].text).toBe(text);
                    done();
                }).catch((err) => done(err));
            });
    });

    it('should not create todo with invalid body data', (done) => {
        request(app)
            .post('/todos')
            .send({})
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                Todo.find().then((todos) => {
                    expect(todos.length).toBe(3);
                    done();
                }).catch((err) => done(err));
            });
    });
});

describe('Get /Todos', () => {
    it('should get all todos', (done) => {
        request(app)
        .get('/todos')
        .expect(200 )
        .expect((res) => {
            expect(res.body.todos.length).toBe(3);
        })
        .end(done);
    });
});

console.log(todos[0]._Id.toHexString());


describe('Get /Todos/:id', () => {
    it('should return a todo document', (done) => {
        request(app)
        .get(`/todos/${todos[0]._Id.toHexString()}`)
        .expect(200)
        // .expect((res) => {
        //     expect(res.body.todo.text.toBe(todos[0].text));
        // })
        .end(done);
    });
});
